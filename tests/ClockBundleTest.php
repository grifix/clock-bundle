<?php

declare(strict_types=1);

namespace Grifix\MemoryBundle\Tests;

use Grifix\Clock\ClockInterface;
use Grifix\Clock\SystemClock;
use Grifix\ClockBundle\GrifixClockBundle;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class ClockBundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        /**
         * @var TestKernel $kernel
         */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(GrifixClockBundle::class);
        $kernel->handleOptions($options);

        return $kernel;
    }

    public function testItWorks(): void
    {
        $container = self::getContainer();
        self::assertTrue($container->has(ClockInterface::class));
        $clock = $container->get(ClockInterface::class);
        self::assertInstanceOf(SystemClock::class, $clock);
        self::assertInstanceOf(\DateTimeImmutable::class, $clock->getCurrentTime());
    }
}
